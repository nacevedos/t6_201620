package taller.interfaz;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import taller.mundo.Ciudad;
import taller.mundo.Ciudadano;


public class Main {

	private static String rutaEntrada = "./data/ciudadLondres.csv";
	private static Ciudad mundo;
	
	public static void main(String[] args) {
		
		
		//Cargar registros
		
		System.out.println("Bienvenido al directorio de emergencias por colicsiones de la ciudad de Londres");
		System.out.println("Espere un momento mientras cargamos la información...");
		System.out.println("Esto puede tardar unos minutos...");
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File(rutaEntrada)));
			String entrada = br.readLine();

			//TODO: Inicialice el directorio t
			mundo= new Ciudad();
			int i = 0;
			entrada = br.readLine();
			while (entrada != null){
				String[] datos = entrada.split(",");
				//TODO: Agrege los datos al directorio de emergencias
				mundo.agregarCiudadano(datos[0], datos[1], datos[2],datos[3]+","+datos[4]);
				mundo.agregarCiudadanoNombre(datos[0], datos[1], datos[2],datos[3]+","+datos[4]);
				mundo.aregarCiudadadoLocalizacion(datos[0], datos[1], datos[2],datos[3]+","+datos[4]);
				//Recuerde revisar en el enunciado la estructura de la información
				++i;
				if (++i%500000 == 0)
					System.out.println(i+" entradas...");

				entrada = br.readLine();
			}
			System.out.println(i+" entradas cargadas en total");
			br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		System.out.println("Directorio de emergencias por colisiones de Londres v1.0");

		boolean seguir = true;

		while (seguir)
			try {
				System.out.println("Bienvenido, seleccione alguna opcion del menú a continuación:");
				System.out.println("1: Agregar ciudadano a la lista de emergencia");
				System.out.println("2: Buscar ciudadano por número de contacto del acudiante");
				System.out.println("3: Buscar ciudadano por apellido");
				System.out.println("4: Buscar ciudadano por su locaclización actual");
				System.out.println("Exit: Salir de la aplicación");
				
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				String in = br.readLine();
				String telefono="";
				long time=0;
				switch (in) {
				
				case "1":
					//TODO: Implemente el requerimiento 1.
					//Agregar ciudadano a la lista de emergencia
					System.out.println("Ingrese el nombre del ciudadano: ");
					String nombre= br.readLine();
					System.out.println("Ingrese el apellido del ciudadano: ");
					String apellido= br.readLine();
					System.out.println("Ingrese el telefono del ciudadano: ");
					telefono= br.readLine();
					System.out.println("Ingrese la localizacion del ciudadano (Latitud, Longitud): ");
					String loca= br.readLine();
					mundo.agregarCiudadano(nombre, apellido, telefono,loca);
					mundo.agregarCiudadanoNombre(nombre, apellido, telefono, loca);
					mundo.aregarCiudadadoLocalizacion(nombre, apellido, telefono, loca);
					break;
				case "2":
					//TODO: Implemente el requerimiento 2
					//Buscar un ciudadano por el número de teléfono de su acudiente
					System.out.println("Ingrese el número telefónico.");
					telefono = br.readLine();
					time = System.currentTimeMillis();
					Ciudadano buscado = mundo.buscarCiudadano(telefono);
					if(buscado == null)
						System.out.println("No existe un contacto con el número "+telefono+"en el directorio");
					else
						System.out.println(buscado.toString());
					time = imprimirTiempo(time);
					
					break;
				case "3":
					//TODO: Implemente el requerimiento 3
					//Buscar ciudadano por apellido/nombre
					System.out.println("Ingrese el nombre del ciudadano: ");
					String t1 = br.readLine();
					time = System.currentTimeMillis();
					Ciudadano buscado2 = mundo.buscarCiudadanoNombre(t1);
					if(buscado2 == null)
					{
						System.out.println("No existe un contacto con el nombre " + t1 
								+ " en el directorio.");
					}
					else
					{
						System.out.println(buscado2.toString());
					}
					time = imprimirTiempo(time);

					break;
				case "4":
					//TODO: Implemente el requerimiento 4
					//Buscar ciudadano por su locaclización actual
					System.out.println("Ingrese localización del ciudadano (Latitud, Longitud): ");
					String local = br.readLine();
					String[] datos = local.split(",");
					double u = Double.parseDouble(datos[0]);
					double d = Double.parseDouble(datos[1]);
					int hash = (int) (u*31 + d*31);
					time = System.currentTimeMillis();
					Ciudadano buscado3 = mundo.buscarCiudadanoLoca(hash+"");
					if(buscado3 == null)
					{
						System.out.println("No existe un contacto con el nombre " + local 
								+ " en el directorio.");
					}
					else
					{
						System.out.println(buscado3.toString());
					}
					time = imprimirTiempo(time);

					break;
				case "Exit":
					System.out.println("Cerrando directorio...");
					seguir = false;
					break;

				default:
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}


	}

	private static long imprimirTiempo(long time) {
		// TODO Auto-generated method stub
		double milis = System.currentTimeMillis()-time;
		System.out.println("Tiempo: "+milis+" milis.");
		return System.currentTimeMillis();
	}

}

package taller.test;

 
import junit.framework.TestCase;
import taller.estructuras.NodoHash;
import taller.estructuras.TablaHash;

public class TablaHashTest extends TestCase{

	private TablaHash<String, String> tablaHash;

	private void setupEscenario1(){
		//Escenario vacio
		tablaHash = new TablaHash<String, String>();
	}
	
	private void setupEscenario2(){
		//Escenario vacio
		tablaHash = new TablaHash<String, String>();
		tablaHash.put("0", "a");
		tablaHash.put("1001", "b");
		tablaHash.put("5", "c");
		tablaHash.put("5", "d");
		tablaHash.put("2003", "e");
		tablaHash.put("2003", "f");
	}

	public void testPut(){
		setupEscenario1();

		//Agregar un elemento en un espacio vacio
		tablaHash.put("0", "a");
		NodoHash[] tabla = tablaHash.darTablaHash();
		assertEquals("a", tabla[0].getValor());

		//Agregar un elemento en un espacio mayor a M
		tablaHash.put("1001", "b");
		tabla = tablaHash.darTablaHash();
		assertEquals("b", tabla[1].getValor());

		//Agregar un elemento en un espacio menor a M con colisión
		tablaHash.put("5", "c");
		tablaHash.put("5", "d");
		assertEquals("d", tabla[5].getValor());
		assertEquals("c", tabla[5].getNextNode().getValor());

		//Agregar un elemento en un espacio mayor a M con colisión
		tablaHash.put("2003", "e");
		tablaHash.put("2003", "f");
		assertEquals("f", tabla[3].getValor());
		assertEquals("e", tabla[3].getNextNode().getValor());


	}

	public void testPutResize(){
		setupEscenario1();

		//Agregar 999999 elementos
		int i = 0;
		while(i < 999999){
			tablaHash.put(i + "", i + "");
			i++;
		}
		NodoHash[] tabla = tablaHash.darTablaHash();
		assertEquals(999999, tablaHash.count());
		assertEquals(1000, tablaHash.darCapacidad());
		assertEquals("999998", tabla[998].getValor());

		//Agregar elemento 1000000 y resize
		tablaHash.put(999999 + "", 999999 + "");
		tabla = tablaHash.darTablaHash();
		assertEquals(1000000, tablaHash.count());
		assertEquals(2000, tablaHash.darCapacidad());
		assertEquals("999999", tabla[999].getValor());
	}

	public void testGet(){
		setupEscenario2();
		
		assertEquals("a", tablaHash.get("0"));
		assertEquals("b", tablaHash.get("1001"));
		assertEquals("d", tablaHash.get("5"));
		assertEquals("f", tablaHash.get("2003"));
		assertEquals(null, tablaHash.get("2004"));
	}
	
	public void testDelete(){
		
		setupEscenario2();
		
		NodoHash[] tabla = tablaHash.darTablaHash();
		
		tablaHash.delete("0");
		assertEquals(null, tabla[0]);
		tablaHash.delete("5");
		assertEquals("c", tablaHash.get("5"));
	}
}

package taller.estructuras;

import java.util.Iterator;

public class TablaHash<K extends Comparable<K> ,V> {

	//TODO Una enumeracioón que representa los tipos de colisiones que se pueden manejar
	public static enum Colisiones{
		LINEAR_PROBING, SEPARATE_CHAINING
	}

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	
	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoHash<K, V>[] tablaHash;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	public NodoHash<K, V>[] darTablaHash(){
		return tablaHash;
	}
	
	public int count(){
		return count;
	}
	
	public int darCapacidad(){
		return capacidad;
	}
	@SuppressWarnings("unchecked")
	public TablaHash(){
		capacidad = 1000;
		factorCargaMax = capacidad * 1000;
		tablaHash = new NodoHash[capacidad];
		count = 0;
	}

	@SuppressWarnings("unchecked")
	public TablaHash(int capacidad, float factorCargaMax) {
		this.capacidad = capacidad;
		this.factorCargaMax = factorCargaMax;
		tablaHash = new NodoHash[capacidad];
		count = 0;
	}

	public void put(K llave, V valor){
		//TODO: Gaurde el objeto valor dado por parametro el cual tiene la llave,
		//tenga en cuenta que puede o no puede haber colisiones
		int pos = hash(llave);
		NodoHash<K,V> nuevo = new NodoHash(llave, valor);
		if(tablaHash[pos] != null){
			NodoHash<K, V> temp = tablaHash[pos];
			tablaHash[pos] = nuevo;
			tablaHash[pos].setNextNode(temp);
			count ++;
		}
		else{
			tablaHash[pos] = nuevo;
			count ++;
		}
		
		if(count >= factorCargaMax){
			resize();
		}
	}

	public V get(K llave){
		//TODO: Busque y retorne el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V resp = null;
		int pos = hash(llave);
		if(tablaHash[pos] != null){
			boolean encontrado = false;
			Iterator<NodoHash<K, V>> ite = tablaHash[pos].iterator();
			while(ite.hasNext() && encontrado == false){
				NodoHash<K, V> actual = ite.next();
				if(actual.getLlave().compareTo(llave) == 0){
					resp = actual.getValor();
					encontrado = true;
				}
			}
		}
		return resp;
	}

	public V delete(K llave){
		//TODO: borra el objeto cuya llave es la dada por parametro. Tenga en cuenta
		// colisiones
		V resp = null;
		int pos = hash(llave);
		if(tablaHash[pos] != null){
			Iterator<NodoHash<K, V>> ite = tablaHash[pos].iterator();
			boolean eliminado = false;
			NodoHash<K, V> anterior = null;
			while(ite.hasNext() && eliminado == false){
				NodoHash<K, V> actual = ite.next();
				if(actual.getLlave().compareTo(llave) == 0){
					if(anterior != null){
						anterior.setNextNode(anterior.getNextNode().getNextNode());
					}
					else{
						tablaHash[pos] = actual.getNextNode();
					}
					count --;
					eliminado = true;
				}
				anterior = actual;
			}
		}
		return resp;
	}

	//Hash
	private int hash(K llave)
	{
		int resp = 0;
		String key = llave.toString();
		String nuevo = "";
		for (int i = 0; i < key.length(); i++) {
			nuevo += Character.getNumericValue(key.charAt(i));
		}
		if (Float.parseFloat(nuevo)>Integer.MAX_VALUE)
		{
			resp=Integer.MAX_VALUE;
		}
		else{
		resp = Integer.parseInt(nuevo);
		}
		return resp % capacidad;
//		for (int i = 0; i < key.length(); i++) {
//			resp = (31 * resp + key.charAt(i)) % capacidad;
//		}
//      return resp;
	}
	private void resize(){
		capacidad *= 2;
		NodoHash<K, V>[] nueva = new NodoHash[capacidad];
		for (int i = 0; i < tablaHash.length; i++) {
			nueva[i] = new NodoHash<K, V>( tablaHash[i].getLlave(), tablaHash[i].getValor());
		}
		tablaHash = nueva;
	}
}
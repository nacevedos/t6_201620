package taller.mundo;
import taller.estructuras.TablaHash;

public class Ciudad {
	/**
	 * 
	 */
	private TablaHash<String ,Ciudadano> ciudadanos;
	private TablaHash<String ,Ciudadano> ciudadanos1;
	private TablaHash<String ,Ciudadano> ciudadanos2;
	
	/**
	 * 
	 */
	public Ciudad(){
		ciudadanos = new TablaHash<String ,Ciudadano>();
		ciudadanos1 = new TablaHash<String ,Ciudadano>();
		ciudadanos2 = new TablaHash<String ,Ciudadano>();
	}
	
	/**
	 * 
	 * @return
	 */
	public TablaHash<String ,Ciudadano> darHabitantes(){
		return ciudadanos;
	}
	
	/**
	 * 
	 * @param pNombre
	 * @param pApellido
	 * @param pTelefono
	 * @param loca 
	 */
	public void agregarCiudadano(String pNombre, String pApellido, String pTelefono, String loca){
		ciudadanos.put(pTelefono, new Ciudadano(pNombre, pApellido, Integer.parseInt(pTelefono)));
	}
	public void agregarCiudadanoNombre(String pNombre, String pApellido, String pTelefono, String pLoca)
	{
		Ciudadano c = new Ciudadano(pNombre, pApellido, Integer.parseInt(pTelefono));
		ciudadanos1.put(pNombre, c);
	}
	/**
	 * 
	 * @param pTelefono
	 * @return
	 */
	public Ciudadano buscarCiudadano(String pTelefono){
		return ciudadanos.get(pTelefono);
	}
	public Ciudadano buscarCiudadanoNombre(String pTelefono){
		return ciudadanos1.get(pTelefono);
	}
	public Ciudadano buscarCiudadanoLoca(String pTelefono){
		return ciudadanos2.get(pTelefono);
	}

	public void aregarCiudadadoLocalizacion(String pNombre, String pApellido, String pTelefono, String pLoca) {
		// TODO Auto-generated method stub
		Ciudadano c = new Ciudadano(pNombre, pApellido, Integer.parseInt(pTelefono));
		String[] datos = pLoca.split(",");
		double u = Double.parseDouble(datos[0]);
		double d = Double.parseDouble(datos[1]);
		int hash = (int) (u*31 + d*31);
		ciudadanos2.put(hash+"", c);
	}
	

}

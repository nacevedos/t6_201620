7)
* En este caso cuando se agrega un elemento la colisi�n se hace bajo el m�todo de  SEPARATE_CHAINING, lo que significa que es agregada en forma de lista encadenada en el nodo de la tabla correspondiente a la llave. 
* M: capacidad de la tabla./N: numero de datos.
-Para este caso el factor de carga es el promedio de objetos por lista. 
-La complejidad del peor caso es O(N).
-Sin embargo como sabemos que tendr� una distribuci�n uniforme(n�meros no repetidos) la complejidad promedio sera O(N/M).
-El facto de carga sera <= N y si la lista esta distribuida de manera eficiente N/M.  
Por esto el criterio de crecimiento deber�a mantener un factor de carga que haga que el caso promedio no tarde m�s de un tiempo Optimo(para mi 0.3ms). tambi�n hay que entender las necesidades del cliente si los recursos que tenemos disponibles son de tiempo(mayor carga y menor tama�o) o espacio(menor carga y mayor tama�o tabla);
* La funci�n de hash para manejar colisiones que fue escogida fue SEPARATE_CHAINING, ya que las llaves(n�meros de tel�fono) no se repiten, esta organizaci�n permite un mejor aprovechamiento del espacio y de un buen tiempo de b�squeda, adici�n y eliminaci�n.
8)
R1
|  Entradas |   tiempo(ms) |
|___________|______________|
|  500000   |     250.0    |
|  1000000  |     469.0    |
|  1500000  |     641.0    |                                   
|  1999998  |     2186.0   |                            
|___________|______________|
|  Entradas |   tiempo(ms) |
|___________|______________|
|  500000   |     250.0    |
|  1000000  |     469.0    |
|  1500000  |     640.0    |
|  1999998  |     2172.0   |
|___________|______________|
|  Entradas |   tiempo(ms) |
|___________|______________|
|  500000   |     250.0    |
|  1000000  |     468.0    |
|  1500000  |     640.0    |
|  1999998  |     2172.0   |
|___________|______________|


Se puede ver que como la complejidad nunca supera O(n), el crecimiento al aumentar mucho los datos, no es mucho mayor sino que por el contrario tienen un crecimeinto masomenos lineal. ( Aveces un poco menor, ya que el caso de O(n) es el peor caso) 
SEGUNDA PARTE

1) Para este caso tiene sentido hacer una tabla que contenga listas, para que cuando se busque por una llave se pueda retornar una lista de donde se pueden obtener los diferentes valores. 

10) las tablas de hash no son eficientes en muchos caso cuando se quiere optimizar el uso del espacio, o cuando se quiere iterar sobre muchos elementos de la tabla que debido a la construcci�n de la tabla lo hace mas complicado que con otras estructuras. (por ejemplo encontrar el elemento mas grande seria una operaci�n costosa) 


Nicol�s Acevedo 201530726